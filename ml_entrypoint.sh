#!/bin/bash
# Backwards compatibility for reverse proxy configuration
echo "Info: Now starting the mainzelliste container";
if [ -n "$ML_REVERSEPROXY_FQDN" ]; then
  export TOMCAT_REVERSEPROXY_FQDN="$ML_REVERSEPROXY_FQDN";
  export TOMCAT_REVERSEPROXY_SSL="$ML_REVERSEPROXY_SSL";
  export TOMCAT_REVERSEPROXY_PORT="$ML_REVERSEPROXY_PORT";
  export TOMCAT_REVERSEPROXY_SCHEME="$ML_REVERSEPROXY_SCHEME";
fi

## backward compatibility for docker secrets
if [ -e "/run/secrets/mainzellisteDbName" ]; then \
	export ML_DB_NAME=$(cat /run/secrets/mainzellisteDbName) \
;fi && if [ -e "/run/secrets/mainzellisteDbUser" ]; then \
	export ML_DB_USER=$(cat /run/secrets/mainzellisteDbUser) \
;fi && if [ -e "/run/secrets/mainzellisteDbPassword" ]; then \
	export ML_DB_PASS=$(cat /run/secrets/mainzellisteDbPassword) \
;fi && if [ -e "/run/secrets/mainzellisteApiKey" ]; then \
	export ML_API_KEY=$(cat /run/secrets/mainzellisteApiKey) \
;fi

if [ -f "/run/secrets/mainzellisteConfig" ]; then
  echo "Since mainzelliste 1.10, the official image changed the processing of user provided configurations."
  echo "For now, we will use the configuration as provided by you and ignore all environment variables."
  echo "If you want to use the new user provided environment variables, please provide your configuration with the secret 'mainzelliste.docker.conf' instead of 'mainzellisteConfig'"
  cp /run/secrets/mainzellisteConfig $CATALINA_HOME/webapps/ROOT/WEB-INF/classes/mainzelliste.conf
  rm $CATALINA_HOME/webapps/ROOT/WEB-INF/classes/mainzelliste.docker.conf
  export MANDATORY_VARIABLES=""
fi

# Run the entrypoint of the base image
source /docker/tomcat_entrypoint.sh
