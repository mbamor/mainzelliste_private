/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste.configuration.permission;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ExtendedPermissions {

  private final Map<String, AllowedValues> permissionsMap = new HashMap<>();

  public void add(String name, String value) {
    permissionsMap.compute(name, (k, v) -> {
      if (v == null) {
        v = new AllowedValues(value);
      } else {
        v.addValue(value);
      }
      return v;
    });
  }

  public AllowedValues get(String name) {
    return permissionsMap.get(name);
  }

  public boolean checkPermission(String name, String value) {
    AllowedValues allowedValues = permissionsMap.get(name.trim());
    return allowedValues != null && allowedValues.checkPermission(value);
  }

  public int size() {
    return permissionsMap.size();
  }

  public boolean isEmpty(){
    return permissionsMap.isEmpty();
  }

  public static class AllowedValues {

    private List<String> values = new ArrayList<>();
    private final boolean allowAny;

    public AllowedValues(String values) {
      String valuesAsString = values.trim();
      this.allowAny = valuesAsString.equals("*");
      if (!allowAny && !valuesAsString.isEmpty()) {
        this.values = new ArrayList<>(Arrays.asList(values.split("&")));
      }
    }

    public boolean checkPermission(String value) {
      return allowAny || value != null && values.contains(value.trim());
    }

    public void addValue(String value) {
      if (allowAny || value == null || value.trim().isEmpty() || values.contains(value.trim())) {
        return;
      }
      values.add(value.trim());
    }

    public List<String> getValues() {
      return values;
    }

    public boolean allowAny() {
      return allowAny;
    }
  }
}
