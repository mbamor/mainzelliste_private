/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste.dto;

import de.pseudonymisierung.mainzelliste.AssociatedIds;
import de.pseudonymisierung.mainzelliste.ID;
import de.pseudonymisierung.mainzelliste.Patient;
import de.pseudonymisierung.mainzelliste.blocker.BlockingKey;
import de.pseudonymisierung.mainzelliste.exceptions.InternalErrorException;
import de.pseudonymisierung.mainzelliste.webservice.JobsResource.AddPatientsJobTask;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

public class StatelessRepository implements Repository {

  List<AddPatientsJobTask> jobTasks;
  Map<ID, List<AddPatientsJobTask>> jobTasksByIds;
  Map<ID, List<AddPatientsJobTask>> jobTasksByAssociatedIds;
  Map<BlockingKey, List<AddPatientsJobTask>> jobTasksByBlockingKeys;

  public StatelessRepository(List<AddPatientsJobTask> jobTasks) {
    this.jobTasks = new ArrayList<>(jobTasks.size());
    this.jobTasksByIds = new ConcurrentHashMap<>(jobTasks.size());
    this.jobTasksByAssociatedIds = new ConcurrentHashMap<>(jobTasks.size() * 4);
    this.jobTasksByBlockingKeys = new ConcurrentHashMap<>(jobTasks.size() * 3);
    init(jobTasks);
  }

  public void init(List<AddPatientsJobTask> jobTasks) {

    for (AddPatientsJobTask jobTask : jobTasks) {
      if (jobTask.getPatient() == null) {
        continue;
      }
      this.jobTasks.add(jobTask);

      // init jobTasksByIds
      for (ID id : jobTask.getPatient().getIds()) {
        jobTasksByIds.compute(id, (k, v) -> {
          if (v == null) {
            v = new ArrayList<>();
          }
          v.add(jobTask);
          return v;
        });
      }

      // init jobTasksByAssociatedIds
      for (AssociatedIds associatedIds : jobTask.getPatient().getAssociatedIdsList()) {
        for (ID id : associatedIds.getIds()) {
          jobTasksByAssociatedIds.compute(id, (k, v) -> {
            if (v == null) {
              v = new ArrayList<>();
            }
            v.add(jobTask);
            return v;
          });
        }
      }

      //init patientByBlockingKeys
      for (BlockingKey blockingKey : jobTask.getPatient().getBlockingKeys()) {
        jobTasksByBlockingKeys.compute(blockingKey, (k, v) -> {
          if (v == null) {
            v = new ArrayList<>();
          }
          v.add(jobTask);
          return v;
        });
      }
    }
  }

  @Override
  public Patient getPatient(ID id, int currentIndex, boolean loadBlockingKeys) {
    List<Patient> result = new ArrayList<>();
    for (AddPatientsJobTask request : jobTasksByIds.get(id)) {
      if (request.index < currentIndex) {
        result.add(request.getPatient());
      }
    }
    if (result.size() > 1) {
      throw new InternalErrorException("Found more than one patient with ID: " + id);
    }
    return result.isEmpty() ? null : result.get(0);
  }

  @Override
  public List<Patient> getPatientsWithAssociatedIds(List<AssociatedIds> associatedIdsList,
      int currentIndex, boolean loadBlockingKeys) {
    List<ID> ids = new ArrayList<>();
    for (AssociatedIds associatedIds : associatedIdsList) {
      ids.addAll(associatedIds.getIds());
    }
    if (ids.isEmpty()) {
      return Collections.emptyList();
    }

    List<Patient> result = new ArrayList<>();
    for (ID id : ids) {
      for (AddPatientsJobTask request : jobTasksByAssociatedIds.get(id)) {
        if (request.index < currentIndex) {
          result.add(request.getPatient());
        }
      }
    }
    return result;
  }

  @Override
  public List<Patient> getPatients(Set<BlockingKey> bks, int currentIndex) {
    List<Patient> result = new ArrayList<>();
    if (bks.isEmpty()) {
      findPatientsInJobTasksByIndex(jobTasks, currentIndex, result);
    } else {
      for (BlockingKey bk : bks) {
        findPatientsInJobTasksByIndex(jobTasksByBlockingKeys.get(bk), currentIndex, result);
      }
    }
    return result;
  }

  private static void findPatientsInJobTasksByIndex(List<AddPatientsJobTask> jobTasks, int currentIndex,
      List<Patient> result) {
    for (AddPatientsJobTask request : jobTasks) {
      if (request.index < currentIndex) {
        result.add(request.getPatient());
      }
    }
  }
}
