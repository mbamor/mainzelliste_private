/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.pseudonymisierung.mainzelliste.util;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.fhirpath.IFhirPath;
import ca.uhn.fhir.parser.DataFormatException;
import ca.uhn.fhir.parser.IParser;
import de.pseudonymisierung.mainzelliste.Config;
import de.pseudonymisierung.mainzelliste.ID;
import de.pseudonymisierung.mainzelliste.IDGenerator;
import de.pseudonymisierung.mainzelliste.IDGeneratorFactory;
import de.pseudonymisierung.mainzelliste.exceptions.InternalErrorException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hl7.fhir.r4.model.DomainResource;
import org.hl7.fhir.r4.model.Encounter;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.PrimitiveType;
import org.hl7.fhir.r4.model.ResourceType;

public class FHIRReader<T extends DomainResource> {

  private FhirContext ctx;
  private IFhirPath fhirPath;
  private final Logger logger = LogManager.getLogger(FHIRReader.class);
  private T resource = null;

  public FHIRReader(FhirContext ctx) {
    if (ctx != null) {
      this.ctx = ctx;
    } else {
      this.ctx = FhirContext.forR4();
    }
    this.fhirPath = this.ctx.newFhirPath();
  }

  /**
   * Parse a FHIR Resource from the supplied body String based on the content type.
   *
   * @param contentType String HTTP header Content-Type value.
   * @param body String the body containing a FHIR Resource in the format specified in contentType.
   * @param type Class<T> Class of the FHIR HAPI Resource that will be used to parse the Resource.
   *     This should be the same Class that is used as a type for this FHIRReader instance.
   * @return T FHIR Resource of `type`. The resource is held internally and can be retrieved using
   *     getResource() afterwards.
   * @throws DataFormatException
   */
  public T parseResource(String contentType, String body, Class<T> type)
      throws DataFormatException {
    FHIRWriter writer = new FHIRWriter(this.ctx);
    IParser parser = writer.setParser(contentType, null);

    this.resource = parser.parseResource(type, body);
    return this.resource;
  }

  /**
   * Set a FHIR Resource directly without parsing it, e.g. when it has been parsed otherwise already.
   * @return
   */
  public T setResource(T resource) {
    this.resource = resource;
    return this.resource;
  }

  /**
   * Return the resource that is parsed using this FHIRReaders `parseResource()` method.
   *
   * @return T the last resource that has been parsed.
   */
  public T getResource() {
    return this.resource;
  }

  /**
   * Retrieve a Map of inputFieldName -> value based on the configured FHIRPath mapping in this
   * Mainzelliste instance. Values are retrieved using FHIRPath from `patient`. If the Resource
   * stored internally is not of type Patient, an InternalErrorException will be thrown.
   *
   * @return Map<String, String> A Map of inputFieldName -> value entries, as would have been
   *     supplied by the traditional Mainzelliste formular API.
   */
  public Map<String, String> readPatientInputFields() throws InternalErrorException {
    if (!this.resource.getResourceType().equals(ResourceType.Patient)) {
      throw new InternalErrorException(
          "Trying to apply Patient method on a non-Patient FHIR Resource.");
    }
    // Get fhir mapping and terser
    Map<String, String> fhirMapping = Config.instance.getFHIRMapping();

    Map<String, String> fhirInputFields = new HashMap<>();
    for (Map.Entry<String, String> entry : fhirMapping.entrySet()) {
      logger.debug(entry.getValue());
      List<PrimitiveType> fhirFields =
          this.fhirPath.evaluate(this.resource, entry.getValue(), PrimitiveType.class);
      PrimitiveType<?> fhirField = null;
      for (PrimitiveType<?> field : fhirFields) {
        // check if the value at the same path has different values
        if (fhirField != null && !field.getValueAsString().equals(fhirField.getValueAsString())) {
          logger.debug("Encountered inconsistent value for FHIRPath mapping " + entry.getValue());
        } else {
          fhirField = field;
        }
      }
      if (fhirField != null) {
        if (entry.getKey().startsWith("date.")) {
          // Get the config associated with this datefield
          String dateFieldKeys =
              Config.instance.getProperty("validator." + entry.getKey() + ".fields");
          String dateFormatString =
              Config.instance.getProperty("validator." + entry.getKey() + ".format");

          // Split the format String into its parts and parse for each field
          String[] fieldFormatStrings = this.splitOnCharChange(dateFormatString);
          String[] fieldNames = dateFieldKeys.split(",");
          if (fieldNames.length != fieldFormatStrings.length) {
            logger.warn("Cannot convert date format of " + entry.getKey() + " into its fields.");
          } else {
            for (int i = 0; i < fieldNames.length; i++) {
              SimpleDateFormat sdf = new SimpleDateFormat(fieldFormatStrings[i]);
              fhirInputFields.put(fieldNames[i].trim(), sdf.format(fhirField.getValue()));
            }
          }
        } else {
          fhirInputFields.put(entry.getKey(), fhirField.getValue().toString());
        }
      }
    }
    return fhirInputFields;
  }

  /**
   * Creates a List of IDs from all Identifiers contained in the FHIR Resource.
   *
   * @return List<ID> a List of IDs that are a) available in Mainzelliste and b) contain both System
   *     and Value.
   */
  public List<ID> readIds() {
    List<ID> validIds = new ArrayList<ID>();
    for (Identifier id : this.getIdentifiers()) {
      if (id.getSystem() != null && id.getValue() != null) {
        IDGenerator<?> idgen = IDGeneratorFactory.instance.getGenerator(id.getSystem());
        if (idgen != null) {
          validIds.add(idgen.buildId(id.getValue()));
        }
      }
    }
    return validIds;
  }

  /**
   * Create a List of Strings containing all requested IDs in the FHIR Resource. A ID request is a
   * Identifier with a System but without a Value.
   *
   * @return List<String> a List of ID-System Strings that are a) available in Mainzelliste and b)
   *     contain only a System and no Value.
   */
  public List<String> readIdRequests() {
    List<String> idRequests = new ArrayList<>();
    for (Identifier id : this.getIdentifiers()) {
      if (id.getSystem() != null && id.getValue() == null) {
        IDGenerator<?> idgen = IDGeneratorFactory.instance.getGenerator(id.getSystem());
        if (idgen != null) {
          idRequests.add(id.getSystem());
        }
      }
    }
    return idRequests;
  }

  /**
   * Retrieves the list of Identifiers from an Encounter or a Patient Resource. Both are retireved
   * based on the ResourceType they broadcast.
   *
   * @return List<Identifier> a List of FHIR Identifier DataTypes that are contained in
   *     this.resource.
   */
  private List<Identifier> getIdentifiers() {
    List<Identifier> identifiers;
    if (this.resource.getResourceType() == ResourceType.Patient) {
      identifiers = ((Patient) this.resource).getIdentifier();
    } else if (this.resource.getResourceType() == ResourceType.Encounter) {
      identifiers = ((Encounter) this.resource).getIdentifier();
    } else {
      logger.error(
          "The parsed FHIR Resource has a non-acceptable type: " + this.resource.getResourceType());
      throw new InternalErrorException("Parsed FHIR Resource has unknown Resource Type.");
    }
    return identifiers;
  }

  /**
   * Splits a String on every change of a character. Useful to split a formatting String into its
   * parts.
   *
   * @param s The String to split
   * @return An array containing each splitted part from s.
   */
  private String[] splitOnCharChange(String s) {
    List<String> stringParts = new ArrayList<String>();
    StringBuilder sb = new StringBuilder();
    char lastChar = 0;
    for (char c : s.toCharArray()) {
      if (c != lastChar && lastChar != 0) {
        stringParts.add(sb.toString());
        sb = new StringBuilder();
      }
      sb.append(c);
      lastChar = c;
    }
    stringParts.add(sb.toString());
    sb = null;
    String[] splitted = new String[stringParts.size()];
    return stringParts.toArray(splitted);
  }
}
