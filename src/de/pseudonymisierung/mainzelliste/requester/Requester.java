package de.pseudonymisierung.mainzelliste.requester;

import de.pseudonymisierung.mainzelliste.Config;
import de.pseudonymisierung.mainzelliste.auth.authenticator.Authenticator;
import de.pseudonymisierung.mainzelliste.auth.credentials.ClientCredentials;
import de.pseudonymisierung.mainzelliste.configuration.permission.ExtendedPermissions;
import de.pseudonymisierung.mainzelliste.exceptions.InvalidConfigurationException;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

/**
 * Represents a Requester could be a UserGroup or a Server
 */

public abstract class Requester {

  /**
   * permissions of the user
   */
  protected Set<String> permissions;
  protected Map<String, ExtendedPermissions> refinedPermissions = new HashMap<>();
  /**
   * Authentication method of the user
   */
  protected Authenticator authenticator;
  protected String id;
  protected String name;

  /**
   * Creates a new Requester, with his permissions and authentication method, creates a random ID
   *
   * @param permissions   List of the permissions
   * @param authenticator Authentication method of the User
   */
  public Requester(Set<String> permissions, Authenticator authenticator) {
    this(permissions, authenticator, UUID.randomUUID().toString());
  }

  /**
   * Creates a new Requester, with his permissions and authentication method, creates a random ID
   *
   * @param permissions   List of the permissions
   * @param authenticator Authentication method of the User
   * @param id            The id of the requester
   */
  public Requester(Set<String> permissions, Authenticator authenticator, String id) {
    this(permissions, authenticator, id, id);
  }

  /**
   * Creates a new Requester, with his permissions and authentication method, creates a random ID
   *
   * @param permissions   List of the permissions
   * @param authenticator Authentication method of the User
   * @param id            The id of the requester
   * @param name          The name of the requester
   */
  public Requester(Set<String> permissions, Authenticator authenticator, String id, String name) {
    this.permissions = permissions;
    this.refinedPermissions = deserializeExtendedPermissions(permissions, Config.instance.isExtendedPermissionCheckCaseSensitive());
    this.authenticator = authenticator;
    this.id = id;
    this.name = name;
  }

  public Map<String, ExtendedPermissions> deserializeExtendedPermissions(Set<String> permissionDefinitions,
      boolean extendedPermissionCheckCaseSensitive) {
    Map<String, ExtendedPermissions> result = new HashMap<>();
    for (String permissionDefinition : permissionDefinitions) {
      permissionDefinition = permissionDefinition.trim();
      int startSemicolonIndex = permissionDefinition.indexOf("{");
      int endSemicolonIndex = permissionDefinition.indexOf("}");
      //ignore if not an extended permission definition
      if (startSemicolonIndex < 1 && endSemicolonIndex < 1) {
        continue;
      } else if (startSemicolonIndex < 1 || endSemicolonIndex < 1) {
        throw new InvalidConfigurationException("Syntax error parsing Refined Permission: Missing"
            + (startSemicolonIndex < 1 ? '{' : '}') + " . parsed permission : " + permissionDefinitions);
      }

      String permissionName = permissionDefinition.substring(0, startSemicolonIndex).trim();
      //TODO validate permissionName ?
      permissionDefinition = permissionDefinition.substring(startSemicolonIndex + 1, endSemicolonIndex);
      for (String permissionElement : permissionDefinition.trim().split("\\|")) {
        if(permissionElement.trim().isEmpty())
          continue;
        String[] permissionElementEntry = permissionElement.trim().split(":");
        if (permissionElementEntry[0].trim().isEmpty() ) {
          throw new InvalidConfigurationException("Syntax error parsing Refined Permission: permission"
              + " name is required '" + permissionElement + "' . parsed permission : "
              + permissionDefinitions);
        }

        result.compute(permissionName, (k,v) -> {
          if(v == null)
            v = new ExtendedPermissions();
          String parameterName = extendedPermissionCheckCaseSensitive ? permissionElementEntry[0].trim():
              permissionElementEntry[0].toLowerCase().trim();
          String parameterValue = "";
          if(permissionElementEntry.length > 1)
            parameterValue = extendedPermissionCheckCaseSensitive ? permissionElementEntry[1].trim():
              permissionElementEntry[1].toLowerCase().trim();
          v.add(parameterName, parameterValue);
          return v;
        });
        // add default value of alloweduses
        String allwedUsesString = extendedPermissionCheckCaseSensitive ? "allowedUses": "alloweduses";
        Optional.ofNullable(result.get(permissionName))
            .filter(p -> p.get(allwedUsesString) == null)
            .ifPresent( p -> p.add(allwedUsesString, "1"));
      }
    }
    return result;
  }

  public Requester() {
  }

  /**
   * Returns a List with all permissions of the Requester
   *
   * @return List of permissions
   */
  public Set<String> getPermissions() {
    return this.permissions;
  }


  public Map<String, ExtendedPermissions> getExtendedPermissions() {
    return refinedPermissions;
  }

  /**
   * Checks if a Requester could been authenticated
   *
   * @param authentication the claims to authenticate
   * @return true if the requester could be authenticated, false if not
   */
  public boolean isAuthenticated(ClientCredentials authentication) {
    return this.authenticator.isAuthenticated(authentication);
  }

  /**
   * Returns the id of the Requester
   *
   * @return Returns the id
   */
  public String getId() {
    return this.id;
  }

  /**
   * Returns the name of the requester
   *
   * @return Returns the name
   */
  public String getName() {
    return this.name;
  }

  @Override
  public String toString() {
    return "Requester{" +
        "permissions=" + permissions +
        ", authenticator=" + authenticator +
        ", id='" + id + '\'' +
        ", name='" + name + '\'' +
        '}';
  }
}
