/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste.crypto;

import com.google.crypto.tink.HybridEncrypt;
import com.google.crypto.tink.hybrid.HybridConfig;
import de.pseudonymisierung.mainzelliste.crypto.key.CryptoKey;
import de.pseudonymisierung.mainzelliste.exceptions.InternalErrorException;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import org.apache.commons.codec.binary.Base64;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Hybrid encryption with Tink (ECIES with AEAD and HKDF). The plaintext is encrypted with a new
 * generated symmetric key and the asymmetric public key is used to encrypt the symmetric key only.
 * ciphertext = symmetric ciphertext + encrypted symmetric key.
 */
public class TinkHybridEncryption extends AbstractTinkProvider<HybridEncrypt> implements
    Encryption {

  private static final Logger logger = LogManager.getLogger(TinkHybridEncryption.class);

  static {
    try {
      HybridConfig.register();
    } catch (GeneralSecurityException e) {
      logger.fatal("Couldn't register key managers to handle supported HybridDecrypt "
          + "and HybridEncrypt Tink-key types", e);
      throw new InternalErrorException(e);
    }
  }

  public TinkHybridEncryption(CryptoKey key) throws GeneralSecurityException {
    super(key, HybridEncrypt.class);
  }

  @Override
  public byte[] encrypt(String plaintext) throws GeneralSecurityException {
    return primitive.encrypt(plaintext.getBytes(StandardCharsets.UTF_8), null);
  }

  /**
   * return a URL-safe base 64 cipher text
   *
   * @param plaintext plain text
   * @return resulting a URL-safe base 64 text
   */

  @Override
  public String encryptToBase64String(String plaintext) throws GeneralSecurityException {
    return Base64.encodeBase64URLSafeString(encrypt(plaintext));
  }
}
