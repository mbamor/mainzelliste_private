package de.pseudonymisierung.mainzelliste.model.persistor.search;

import de.pseudonymisierung.mainzelliste.Patient;
import java.util.ArrayList;
import java.util.List;

public class SearchPatientResult {
  List<Patient> patients = new ArrayList<>();
  long totalCount = 0;

  public SearchPatientResult(List<Patient> patients, long totalCount) {
    this.patients = patients;
    this.totalCount = totalCount;
  }

  public List<Patient> getPatients() {
    return patients;
  }

  public long getTotalCount() {
    return totalCount;
  }
}
