/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste.model.persistor.search.filter.sql.common;

import de.pseudonymisierung.mainzelliste.model.persistor.search.filter.sql.common.SqlComparisonSearchCondition.SqlOperator;
import de.pseudonymisierung.mainzelliste.model.persistor.search.filter.sql.mapper.SearchConditionFields;

public class SqlComparisonSearchCondition extends
    AbstractComparisonSearchCondition<String, SqlOperator> {

  public enum SqlOperator implements Operator {
    EQL("=", "="), LIKE("ilike", "like");

    private final String postgresOperator;
    private final String mySqlOperator;

    SqlOperator(String postgresOperator, String mySqlOperator) {
      this.postgresOperator = postgresOperator;
      this.mySqlOperator = mySqlOperator;
    }

    public String getValue(boolean isPostgres) {
      return isPostgres ? postgresOperator : mySqlOperator;
    }
  }

  public SqlComparisonSearchCondition(String value, SqlOperator operator,
      SearchConditionFields searchConditionField) {
    super(value, operator, searchConditionField);
  }

  @Override
  protected String getValueAsString() {
    String wildCard = getOperator() != SqlOperator.LIKE ? "" : "%";
    return wildCard + getValue() + wildCard;
  }
}
