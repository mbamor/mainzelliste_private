package de.pseudonymisierung.mainzelliste.webservice.commons;

import de.pseudonymisierung.mainzelliste.webservice.token.ReadPatientsToken;
import java.util.ArrayList;
import java.util.List;

public class RedirectUtils {

  public static List<String> getRequestedIDsTypeFromToken(ReadPatientsToken token) {
    if (!token.getResultIds().isEmpty()) {
      return new ArrayList<>(token.getResultIds());
    } else if (!token.getPatientSearchIds().isEmpty()) {
      return token.getPatientSearchIds().stream().map(e -> e.getSearchId().getType()).toList();
    }
    return null;
  }
}
