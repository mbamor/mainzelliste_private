/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste.webservice;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import de.pseudonymisierung.mainzelliste.Config;
import de.pseudonymisierung.mainzelliste.ID;
import de.pseudonymisierung.mainzelliste.IDRequest;
import de.pseudonymisierung.mainzelliste.Patient;
import de.pseudonymisierung.mainzelliste.PatientBackend;
import de.pseudonymisierung.mainzelliste.Servers;
import de.pseudonymisierung.mainzelliste.api.AddPatientRequest;
import de.pseudonymisierung.mainzelliste.api.AddPatientsJobTaskResponse;
import de.pseudonymisierung.mainzelliste.api.IdResponse;
import de.pseudonymisierung.mainzelliste.api.JobTaskErrorResponse;
import de.pseudonymisierung.mainzelliste.api.JobTaskResponse;
import de.pseudonymisierung.mainzelliste.dto.CachedRepository;
import de.pseudonymisierung.mainzelliste.dto.Persistor;
import de.pseudonymisierung.mainzelliste.dto.StatelessRepository;
import de.pseudonymisierung.mainzelliste.exceptions.InternalErrorException;
import de.pseudonymisierung.mainzelliste.exceptions.InvalidTokenException;
import de.pseudonymisierung.mainzelliste.matcher.MatchResult;
import de.pseudonymisierung.mainzelliste.matcher.MatchResult.MatchResultType;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import jakarta.inject.Singleton;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.WebApplicationException;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.core.UriInfo;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Resource-based access to jobs. A job is a series of tasks in a batch mode
 */
@Path("/jobs")
@Singleton
public class JobsResource {

  private static final Logger logger = LogManager.getLogger(JobsResource.class);

  private final Map<Long, Job> jobs = new HashMap<>();
  private final Gson gson = new Gson();

  private static class Job {

    private final String tokenId;
    private final Thread thread;
    private String result;
    private Throwable throwable;

    public Job(String tokenId, Function<Job, Runnable> runnableSupplier) {
      this.tokenId = StringUtils.trimToEmpty(tokenId);
      this.thread = new Thread(runnableSupplier.apply(this));
      this.thread.setUncaughtExceptionHandler((t, e) -> {
        throwable = e;
        result = "";
        logger.error(" Job failed. cause:", e);
      });
      this.thread.start();
    }

    public boolean isSuccessful() {
      return throwable == null;
    }

    public void setResult(String result) {
      this.result = result;
    }

    public String getTokenId() {
      return tokenId;
    }

    public long getId() {
      return thread.getId();
    }

    public Throwable getThrowable() {
      return throwable;
    }
  }

  @Path("/{jobId}/")
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response getJobResult(@PathParam("jobId") long jobId,
      @QueryParam("tokenId") String tokenId,
      @Context HttpServletRequest request) {

    Job job = jobs.get(jobId);
    if (job == null) {
      return Response
          .status(Status.NOT_FOUND)
          .entity("job not found")
          .build();
    } else if (!StringUtils.trimToEmpty(tokenId).equals(job.getTokenId())) {
      return Response.status(Status.UNAUTHORIZED)
          .entity("please supply a valid 'addPatients' token")
          .build();
    } else if (job.result == null) {
      return Response
          .status(Status.NO_CONTENT)
          .build();
    } else if (!job.isSuccessful()) {
      jobs.remove(jobId);
      Throwable cause = job.getThrowable();
      if (cause instanceof WebApplicationException) {
        throw (WebApplicationException) cause;
      } else {
        throw new InternalErrorException(cause);
      }
    } else {
      jobs.remove(jobId);
      return Response
          .status(Status.ACCEPTED)
          .entity(job.result)
          .build();
    }
  }

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @Produces(MediaType.APPLICATION_JSON)
  public synchronized Response runJob(
      @QueryParam("tokenId") String tokenId,
      @Context HttpServletRequest request,
      @Context UriInfo context,
      String batchForms) {

    //read Token
    Token token = readToken(tokenId);

    // prepare job function
    Supplier<String> jobFunction;
    if (token instanceof AddPatientsToken) {
      logger.log(Level.getLevel(Constants.LOG_LEVEL_BENCHMARK), "begin deserialization of patients bundle" );
      // deserialize JSON to a list of AddPatientRequest pojo
      List<AddPatientRequest> addPatientRequests = AddPatientRequest.getGson().fromJson( batchForms,
          new TypeToken<List<AddPatientRequest>>() {}.getType());
      logger.log(Level.getLevel(Constants.LOG_LEVEL_BENCHMARK), "deserialization of patients bundle done" );

      // init. add patient requests with index
      AtomicInteger currentRequestIndex = new AtomicInteger();
      List<AddPatientsJobTask> jobRequests = addPatientRequests.stream()
          .map( r -> new AddPatientsJobTask(currentRequestIndex.getAndIncrement(), r))
          .collect(Collectors.toList());
      logger.log(Level.getLevel(Constants.LOG_LEVEL_BENCHMARK), "initialization of add patient job tasks done" );

      // create job function
      jobFunction = () -> addPatients(jobRequests, (AddPatientsToken) token, context);
    } else {
      logger.error("Token {} is not of type 'addPatients' but '{}'", tokenId, token.getType());
      throw new InvalidTokenException("Token type " + token.getType() + " not supported. Please supply a valid token.", Status.UNAUTHORIZED);
    }

    // create and start the job
    Job job = new Job(tokenId, j -> () -> j.setResult(jobFunction.get()));
    jobs.put(job.getId(), job);

    return Response
        .status(Status.ACCEPTED)
        .location(context.getBaseUriBuilder()
            .path("/jobs/{jobId}/")
            .queryParam("tokenId", tokenId)
            .build(job.thread.getId()))
        .build();
  }

  public abstract static class JobTask {
    public final int index;
    public boolean runSequentially = true;
    protected JobTaskResponse result;

    protected JobTask(int index) {
      this.index = index;
    }

    protected void processTask(Runnable task){
      try {
        task.run();
      } catch (WebApplicationException e) {
        this.result = new JobTaskErrorResponse(e.getResponse().getStatus(), e.getResponse().getEntity().toString());
      } catch (RuntimeException e) {
        this.result = new JobTaskErrorResponse(Status.INTERNAL_SERVER_ERROR.getStatusCode(), e.getMessage());
      }
    }
  }

  public static class AddPatientsJobTask extends JobTask {
    private AddPatientRequest restDTO;
    private Patient patient;
    private boolean sureness = false;
    private IDRequest idRequest;

    public AddPatientsJobTask(int index, AddPatientRequest restDTO) {
      super(index);
      this.restDTO = restDTO;
    }

    /**
     * deserialize patient from json DTO to Patient object and read sureness flag
     */
    public void deserializeRequestDTO() {
      processTask(() -> {
        this.patient = PatientBackend.createPatientFrom(restDTO.getFields(), restDTO.getIds(), restDTO.ignoreInvalidIDAT());
        this.sureness = restDTO.isSureness();
      });
      // trying to free memory
      this.restDTO = null;
    }

    /**
     * make sure, that this task can be run concurrently
     */
    public void qualifyTask(PatientBackend patientBackend) {
      try {
        MatchResult matchResult = patientBackend.findMatch(this.patient, null, this.index);
        this.runSequentially = matchResult.getResultType() != MatchResultType.NON_MATCH;
      } catch (WebApplicationException e) {
        this.runSequentially = e.getResponse().getStatus() != Status.CONFLICT.getStatusCode() || !this.sureness;
      } catch (RuntimeException e) {
        //ignore TODO log error
      }
    }

    /**
     * find match and create ID Request
     * @param requestedIdTypes id types
     * @param tokenId token id
     * @param uriInfo required to find base url of mainzelliste
     */
    public void createPatient(PatientBackend patientBackend, RequestedIdTypes requestedIdTypes, String tokenId, UriInfo uriInfo) {
      processTask(() -> {
        this.idRequest = patientBackend.createNewPatient(this.patient, requestedIdTypes, this.sureness, tokenId);
        this.result = idRequestToJson(this.idRequest, uriInfo);
      });
      // trying to free memory
      this.patient = null;
    }

    public void createAndPersistPatient(RequestedIdTypes requestedIdTypes,
        String tokenId, UriInfo uriInfo) {
      processTask(() -> {
        IDRequest currentIdRequest = PatientBackend.getDefaultInstance()
            .createAndPersistPatient(this.patient, requestedIdTypes, this.sureness, tokenId);
        this.result = idRequestToJson(currentIdRequest, uriInfo);
      });
      // trying to free memory
      this.patient = null;
    }

    /**
     * serialize result to json POJO
     * @param response contain result
     * @param uriInfo required to create a URI for every ID json object
     * @return json POJO JobSingleResponse
     */
    private JobTaskResponse idRequestToJson(IDRequest response, UriInfo uriInfo) {
      if (response.getAssignedPatient() != null) {
        List<IdResponse> idResponses = new ArrayList<>();
        for (ID thisID : response.getRequestedIds()) {
          URI uri = uriInfo.getBaseUriBuilder()
              .path(JobsResource.class)
              .path("/{idtype}/{idvalue}")
              .build(thisID.getType(), thisID.getEncryptedIdStringFirst());

          idResponses.add(new IdResponse(thisID.getType(), thisID.getIdString(), thisID.isTentative(), uri.toString()));
        }
        return new AddPatientsJobTaskResponse(Status.CREATED.getStatusCode(), idResponses);
      } else {
        return new JobTaskErrorResponse(Status.CONFLICT.getStatusCode(), "Unable to definitely determined "
            + "whether the data refers to an existing or to a new patient. Please check data or "
            + "resubmit with sureness=true to get a tentative result. Please check documentation "
            + "for details.");
      }
    }

    public Patient getPatient() {
      return patient;
    }
  }

  private String addPatients(List<AddPatientsJobTask> jobTasks, AddPatientsToken token,
      UriInfo uriInfo) {
    logger.log(Level.getLevel(Constants.LOG_LEVEL_BENCHMARK), "begin adding patients asynchronously");
    // deserialize patients from json DTO to Patient object
    jobTasks.parallelStream().forEach(AddPatientsJobTask::deserializeRequestDTO);
    logger.log(Level.getLevel(Constants.LOG_LEVEL_BENCHMARK), "preprocessing of patients done");

    // ensure if every task can be run concurrently
    final PatientBackend patientService = new PatientBackend(new StatelessRepository(jobTasks));
    logger.log(Level.getLevel(Constants.LOG_LEVEL_BENCHMARK), "initialization of stateless repo. done");
    jobTasks.parallelStream()
        .filter(r -> r.patient != null)
        .forEach(t -> t.qualifyTask(patientService));
    logger.log(Level.getLevel(Constants.LOG_LEVEL_BENCHMARK), "pre processing of input done -> "
            + "number of patients that will be processed sequentially = {}",
        jobTasks.parallelStream().filter(r -> r.runSequentially).count());


    patientService.lockCreatingPatient();
    // create patients
    if(Config.instance.isFieldRecordLinkageEnabled()) {
      //Note: getting all necessary patients from database before starting a parallelized record linkage
      patientService.setRepository(new CachedRepository(jobTasks));
      logger.log(Level.getLevel(Constants.LOG_LEVEL_BENCHMARK), "initialization of cached repo. done");
    } else {
      patientService.setRepository(Persistor.instance);
    }
    jobTasks.parallelStream()
        //Note "r.patient != null" is always true, if r.runSynchronized = false
        .filter(r -> !r.runSequentially)
        .forEach(r -> r.createPatient(patientService, token.getRequestedIdTypes(), token.getId(), uriInfo));
    patientService.setRepository(null);
    logger.log(Level.getLevel(Constants.LOG_LEVEL_BENCHMARK), "parallelized match processing done");

    // persist patients
    Persistor.instance.addIdRequests(jobTasks.parallelStream()
        .filter(r -> !r.runSequentially && r.idRequest != null)
        .map(r -> r.idRequest)
        .collect(Collectors.toList()));
    logger.log(Level.getLevel(Constants.LOG_LEVEL_BENCHMARK), "persistence of parallelized matches done");
    patientService.unlockCreatingPatient();

    // create and persist the rest of patients sequentially
    jobTasks.stream()
        .filter(r -> r.runSequentially && r.patient != null)
        .forEach(r -> r.createAndPersistPatient(token.getRequestedIdTypes(), token.getId(), uriInfo));
    logger.log(Level.getLevel(Constants.LOG_LEVEL_BENCHMARK), "adding patients asynchronously done");

    // serialize result to json string
    return gson.toJson(jobTasks.parallelStream()
        .map(r -> r.result)
        .collect(Collectors.toList()));
  }

  /* Helpers */

  /**
   * find token with the given token id and token class
   *
   * @param tokenId token id
   * @return token
   */
  private Token readToken(String tokenId) {
    return Optional.ofNullable(Servers.instance.getTokenByTid(tokenId))
        .orElseThrow(() -> {
          logger.error("No token with id {} found", tokenId);
          return new InvalidTokenException("Please supply a valid 'addPatients' token.",
              Status.UNAUTHORIZED);
        });
  }
}
