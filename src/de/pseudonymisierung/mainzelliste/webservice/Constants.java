package de.pseudonymisierung.mainzelliste.webservice;

public class Constants {

  private Constants(){}

  /**
   * Custom logging level
   */
  public static final String LOG_LEVEL_BENCHMARK = "BENCHMARK";
}
