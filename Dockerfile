ARG DOCKER_COMMON_VERSION=main-tomcat-10
FROM alpine:latest as extract
RUN apk add --no-cache unzip
ADD target/mainzelliste*.war /mainzelliste/mainzelliste.war
RUN mkdir -p /mainzelliste/extracted && \
       unzip /mainzelliste/mainzelliste.war -d /mainzelliste/extracted/

FROM samply/tomcat-common:${DOCKER_COMMON_VERSION}
MAINTAINER t.brenner@dkfz-heidelberg.de
LABEL mainzelliste.version=$SOURCE_BRANCH mainzelliste.commit=$SOURCE_COMMIT
### Values for docker.common that shouldn't be modified
ENV MANDATORY_VARIABLES="ML_DB_PASS ML_API_KEY" COMPONENT="ml"
### Component specific default values
ENV ML_DB_DRIVER="org.postgresql.Driver" ML_DB_TYPE="postgresql" ML_DB_HOST="db" ML_DB_PORT="5432" \
    ML_DB_NAME="mainzelliste" ML_DB_USER="mainzelliste" ML_ALLOWEDREMOTEADDRESSES="0.0.0.0/0" ML_LOG_LEVEL="warn" \
    ML_OIDC_INTERNAL_ISS_BASE_URL=""
ADD --chown=ml:www-data ./ml_entrypoint.sh /docker/ml_entrypoint.sh
RUN chmod +x /docker/ml_entrypoint.sh
COPY --chown=ml:www-data --from=extract /mainzelliste/extracted/ ${CATALINA_HOME}/webapps/ROOT/
ENTRYPOINT ["/docker/ml_entrypoint.sh"]
