/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste.webservice.commons;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.pseudonymisierung.mainzelliste.configuration.permission.ExtendedPermissions;
import de.pseudonymisierung.mainzelliste.requester.RequesterTest.RequesterMockUp;
import de.pseudonymisierung.mainzelliste.webservice.commons.ExtendedPermissionUtils.TokenDataElement;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.codehaus.jettison.json.JSONException;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ExtendedPermissionUtilsTest {

  Map<String, ExtendedPermissions> configuration;

  @BeforeClass
  public void createData() {
    String permissionsConfig = "tt_addPatient{|data.idTypes:pid2|data.resultIds|data.callbackResultIds:pid2|data.redirect:*|data.callback:*};"
        + "tt_readPatients{|data.searchIds.idType:pid2|data.searchIds.idString:*|data.resultFields:vorname&nachname|data.resultFields:geburtstag|data.resultIds:pid|data.resultIds:pid2};";
    Set<String> permissions = new HashSet<>(List.of(permissionsConfig.split(";")));
    configuration = new RequesterMockUp().deserializeExtendedPermissions(permissions,true);
  }

  @Test
  public void testCheckPermission()
      throws JSONException, JsonProcessingException {
    String addPatientTokenString = "{"
        + "    \"type\": \"addPatient\","
        + "    \"data\": {\n"
        + "      \"idTypes\": [\"pid2\"],"
        + "      \"resultIds\": [],"
        + "      \"callbackResultIds\": [\"pid2\"],"
        + "      \"callback\": \"https://postman-echo.com/post\""
        + "    }"
        + "}";
    ExtendedPermissionUtils.checkPermission(desrializeToken(addPatientTokenString), this.configuration.get("tt_addPatient"), true);

    String readPatientTokenString = "{"
        + "    \"type\": \"readPatients\","
        + "    \"data\": {"
        + "        \"searchIds\": ["
        + "           {\"idType\":\"pid2\",\"idString\":\"1KU6Z7YF\"},"
        + "           {\"idType\":\"pid2\",\"idString\":\"FZVCWL4R\"},"
        + "           {\"idType\":\"pid2\",\"idString\":\"97G06K2T\"}"
        + "        ],"
        + "        \"resultFields\": ["
        + "            \"vorname\","
        + "            \"nachname\","
        + "            \"geburtstag\""
        + "        ]"
        + "    }"
        + "}";
    ExtendedPermissionUtils.checkPermission(desrializeToken(readPatientTokenString), this.configuration.get("tt_readPatients"), true);
  }

  @Test
  public void testDeserializeTokenDataElements() throws JSONException, JsonProcessingException {
    String readPatientTokenString = "{"
        + "    \"type\": \"readPatients\","
        + "    \"data\": {"
        + "        \"searchIds\": ["
        + "           {\"idType\":\"pid2\",\"idString\":\"1KU6Z7YF\"},"
        + "           {\"idType\":\"pid2\",\"idString\":\"FZVCWL4R\"},"
        + "           {\"idType\":\"pid2\",\"idString\":\"97G06K2T\"}"
        + "        ],"
        + "        \"resultFields\": ["
        + "            \"vorname\","
        + "            \"nachname\","
        + "            \"geburtstag\""
        + "        ],"
        + "        \"readAllPatientIdTypes\": true"
        + "    }"
        + "}";
    List<TokenDataElement> requestedPermissions = new ArrayList<>();
    ExtendedPermissionUtils.deserializeTokenDataElements("",
        desrializeToken(readPatientTokenString), true, requestedPermissions);
    Assert.assertEquals(requestedPermissions.size(), 10);
    Assert.assertEquals(requestedPermissions.stream().filter(p -> p.getJsonPath().equals("data.searchIds.idType")).count(), 3);
    Assert.assertEquals(requestedPermissions.stream().filter(p -> p.getJsonPath().equals("data.searchIds.idString")).count(), 3);
    Assert.assertEquals(requestedPermissions.stream().filter(p -> p.getJsonPath().equals("data.resultFields")).count(), 3);
    Assert.assertEquals(requestedPermissions.stream().filter(p -> p.getJsonPath().equals("data.readAllPatientIdTypes")).count(), 1);
  }

  private Map<String, Object> desrializeToken(String addPatientTokenString)
      throws JsonProcessingException {
    Map<String, Object> token = new ObjectMapper().readValue(addPatientTokenString, new TypeReference<>() {});
    token.remove("type");
    return token;
  }
}
