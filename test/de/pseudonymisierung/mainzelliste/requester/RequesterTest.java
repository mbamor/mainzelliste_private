/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste.requester;

import de.pseudonymisierung.mainzelliste.configuration.permission.ExtendedPermissions;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RequesterTest {

  @Test
  public void testDeserializeRefinedPermissions() {
    String permissionsConfig = "tt_addPatient{data.idTypes:pid2|data.resultIds|data.callbackResultIds:pid2|data.redirect:*|data.callback:*};"
        + "tt_readPatients{data.searchIds.idType:pid2|data.searchIds.idString:*|data.resultFields:*|data.resultIds:pid|data.resultIds:pid2};";
    Set<String> permissions = new HashSet<>(List.of(permissionsConfig.split(";")));
    Map<String, ExtendedPermissions> refinedPermissions = new RequesterMockUp().deserializeExtendedPermissions(permissions,
        true);

    ExtendedPermissions readPatients = refinedPermissions.get("tt_readPatients");
    Assert.assertEquals(readPatients.size(), 5);
    Assert.assertEquals(readPatients.get("data.searchIds.idType").getValues(), List.of("pid2"));
    Assert.assertTrue(readPatients.get("data.searchIds.idString").allowAny());
    Assert.assertTrue(readPatients.get("data.resultFields").allowAny());
    Assert.assertEquals(readPatients.get("data.resultIds").getValues(), List.of("pid", "pid2"));

    ExtendedPermissions addPatient = refinedPermissions.get("tt_addPatient");
    Assert.assertEquals(addPatient.size(), 6);
    Assert.assertEquals(addPatient.get("data.idTypes").getValues(), List.of("pid2"));
    Assert.assertFalse(addPatient.get("data.resultIds").allowAny());
    Assert.assertTrue(addPatient.get("data.resultIds").getValues().isEmpty());
    Assert.assertEquals(addPatient.get("data.callbackResultIds").getValues(), List.of("pid2"));
    Assert.assertTrue(addPatient.get("data.redirect").allowAny());
    Assert.assertTrue(addPatient.get("data.callback").allowAny());
  }

  public static class RequesterMockUp extends Requester{}
}
