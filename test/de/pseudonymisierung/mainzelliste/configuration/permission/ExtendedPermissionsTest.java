/*
 * Copyright (C) 2013-2015 Martin Lablans, Andreas Borg, Frank Ückert
 * Contact: info@mainzelliste.de
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */
package de.pseudonymisierung.mainzelliste.configuration.permission;

import de.pseudonymisierung.mainzelliste.requester.RequesterTest.RequesterMockUp;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class ExtendedPermissionsTest {

  Map<String, ExtendedPermissions> configuration;

  @BeforeClass
  public void createData() {
    String permissionsConfig = "tt_readPatients{|data.searchIds.idType:pid2|data.searchIds.idString:*"
        + "|data.resultFields:vorname&nachname|data.resultFields:geburtstag|data.resultIds:pid|data.resultIds:pid2};";
    Set<String> permissions = new HashSet<>(List.of(permissionsConfig.split(";")));
    configuration = new RequesterMockUp().deserializeExtendedPermissions(permissions,true);
  }


  @Test
  public void testCheckPermission() {
    ExtendedPermissions extendedPermissions = configuration.get("tt_readPatients");
    //test validation of specific value
    Assert.assertFalse(extendedPermissions.checkPermission("data.searchIds.idType", "kdsd"));
    Assert.assertTrue(extendedPermissions.checkPermission("data.searchIds.idType", "pid2"));

    //test validation of all value allowed (wildcard)
    Assert.assertTrue(extendedPermissions.checkPermission("data.searchIds.idString", "545dsdsd"));
    Assert.assertTrue(extendedPermissions.checkPermission("data.searchIds.idString", ""));

    //test validation of seperated defined permission
    Assert.assertTrue(extendedPermissions.checkPermission("data.resultFields", "vorname"));
    Assert.assertTrue(extendedPermissions.checkPermission("data.resultFields", "nachname"));
    Assert.assertTrue(extendedPermissions.checkPermission("data.resultFields", "geburtstag"));

    // test validation of missing permission
    Assert.assertFalse(extendedPermissions.checkPermission("data.readAllPatientIdTypes", "true"));
    Assert.assertFalse(extendedPermissions.checkPermission("data.readAllPatientIdTypes", "false"));
  }
}
