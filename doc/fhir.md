# Mainzelliste FHIR

This document describes the usage of the Mainzelliste FHIR API. For core functionalities of Mainzelliste refer to the [official documentation](https://www.unimedizin-mainz.de/typo3temp/secure_downloads/22397/0/2c87458bd4452fc79424886e199f11ed1f917bf6/Mainzelliste__Schnittstelle.pdf).

## Notations

In this document, we prefix any entity from the FHIR standard with "FHIR". For example: [FHIR Patient](http://hl7.org/fhir/patient.html) means the FHIR Patient Resource standardized by HL7. In contrast, patient refers to the object defined in Mainzelliste.

`[URL]` describes the [uniform resource locator](https://tools.ietf.org/html/rfc1738) of the Mainzelliste. To make examples more readable, we consider `www.mainzelliste.de` as an example URL for a running Mainzelliste instance.

## Introduction and rationale

_HL7 FHIR is a standard for health care data exchange._ [1](http://hl7.org/fhir/) The FHIR specification defines extensible data structures and communication operations. As the Mainzelliste iteslf is a [RESTful web-service](https://www.w3.org/TR/2004/NOTE-ws-arch-20040211/#relwwwrest) widely used in the health care domain, using HL7 FHIR adds an additional layer of standardization and interoperability.

Mainzelliste curently support three FHIR Resources: [FHIR Patient](http://hl7.org/fhir/patient.html), [FHIR Encounter](http://hl7.org/fhir/encounter.html) and [FHIR Bundle](http://hl7.org/fhir/bundle.html). In addition, we add these RESTful interactions for each FHIR Resource: [FHIR read](http://hl7.org/fhir/http.html#read), [FHIR update](http://hl7.org/fhir/http.html#update), [FHIR create](http://hl7.org/fhir/http.html#create), [FHIR search](http://hl7.org/fhir/http.html#search) as well as [FHIR batch/transaction](http://hl7.org/fhir/http.html#transaction). The latter one applies to FHIR Patient and FHIR Encounter, which are bundled up into a FHIR Bundle. [FHIR delete](http://hl7.org/fhir/http.html#delete) is available but intentionally not implemented.

## Enabling FHIR

 **The FHIR API for Mainzelliste is currently experimental.** To enable usage of the FHIR API in Mainzelliste, you need to set `experimental.fhir.enable = true` in the [Mainzelliste config](https://www.unimedizin-mainz.de/typo3temp/secure_downloads/22397/0/2c87458bd4452fc79424886e199f11ed1f917bf6/Mainzelliste__Konfigurationshandbuch.pdf). Every FHIR REST-endpoint is available if and only if `experimental.fhir.enable` is set.

### Field mapping

As the fields in Mainzelliste can be set individually in each configuration, we need to supply a mapping of each field to a FHIR Resource content. This mapping only applies to FHIR Patient (as the fields only apply to patients as well). Map a field to the FHIR Patient using [FHIRPath](http://hl7.org/fhirpath/N1/) (note that the keys do not have the `experimental.` prefix like the enabling flag has):

```properties
fhir.map.vorname = Patient.name.given
```

This example maps a configured field `vorname` to the given name in FHIR Patient. This is the most simple of mappings. Let us consider a more complex mappings:

```properties
fhir.map.nachname = Patient.name.where(use = 'official').family
fhir.map.geburtsname = Patient.name.where(use = 'maiden').family
```

In this example, `nachname` and `geburtsname` are both mapped to the family name in FHIR patient, but for different name uses. Using the `where` function of FHIRPath - which is the only function that is supported in these mappings - allows you to distinguish two content elements from a FHIR Patient (as name is an array). The `where` function allows the equality operator `=` and the inequality operator `!=`. Note that in the previous example regarding the given name, we did not specify a where clause. In this case, the field `vorname` will be mapped to both FHIR HumanName elements in FHIR Patient, i.e.:

```json
{
    "resourceType": "Patient",
    "name": [
        {
            "use": "official",
            "given": [ "Marcel" ],
            "family": "Parciak"
        }, 
        {
            "use": "maiden",
            "given": [ "Marcel "],
            "family": "ParciakMaiden"
        }
    ]
}
```

Let us consider a mapping of a date. Mainzelliste handles dates as integers and combines them in validators, like `validator.date.0.format = yyyyMMdd`. The FHIR API implementation aims to make use of the validation capabilities and allows to map dates using them:

```properties
fhir.map.date.0 = Patient.birthDate
```

This mapping retrieves all fields specified in `validator.date.0.fields`, tries to parse them using the format `validator.date.0.format` and formats it into the [ISO-8601 datetime format](https://www.iso.org/iso-8601-date-and-time-format.html) FHIR needs.

### FHIR Permissions

As a RESTful API, FHIR allows usage of HTTP header variables like Mainzelliste does to supply `mainzellisteApiKey` for authorization. To grant an API key access to the FHIR API, the FHIR permission Strings have to be configured. A permission String exists for each FHIR interaction (`read`, `update`, `create`, `delete`, `search`), prefixed with `fhir_`:

```properties
servers.0.permissions = createSession;showSessionIds;createToken;tt_addPatient;tt_readPatients;tt_editPatient;fhir_read;fhir_update;fhir_create;fhir_delete;fhir_search
```

In this example, the API key set in `servers.0` would have permission to use all FHIR interactions, i.e. all available FHIR API REST-endpoints. Note that due to the implementation of the FHIR Api, the _traditional_ permissions need to be set, too. For example, the `fhir_create` permission may fail if the API key does not have `tt_addPatient` permissions, as any request received as FHIR is translated into a request in the _traditional_ Mainzelliste API.

## FHIR Resources

Interaction REST-endpoints are available under `[URL] / fhir / [Resource]`, e.g. `www.mainzelliste.de/fhir/Patient`. The FHIR Version used is [R4](http://hl7.org/fhir/r4). Implemented Content-Types are `application/fhir+json`, `application/fhir+xml` and `application/fhir+turtle`. Usually, the default ID Generator (i.e. the first IDGenerator set in the configuration) will serve as the provider of [FHIR Logical IDs](http://hl7.org/fhir/resource.html#id). Due to this fact we strongly discourage changing the default IDGenerator. It may have unintended side effects and is not tested at all.

### Identifiers 

The Mainzelliste aims to handle Identifiers of any kind. Therefore, the [FHIR Identifier Data Type](http://hl7.org/fhir/datatypes.html#Identifier) is handled differently. There is no mapping for Identifiers, any mapping will be ignored. Internally, `use`, `system` and `value` of FHIR Identifier are used to represent any ID in Mainzelliste. FHIR `Identifier.system` represents an ID type, FHIR `Identifier.value` represents an ID string. Assuming we have an identifier of type `pid` with the string `0003Y0WZ`, the FHIR Identifier would look like this:

```json
{
    "system": "pid",
    "value": "0003Y0WZ"
}
```

Assuming that an identifier is tentative (i.e. a possible, but not sure match), the [FHIR IdentifierUse](http://hl7.org/fhir/valueset-identifier-use.html) `temp` is added to the FHIR Identifier. This signals anyone working with the Identifier that it may not be permanent (as it may be merged into another Identifier):

```json
{
    "use": "temp",
    "system": "pid",
    "value": "0003Y0WZ"
}
```

FHIR Identifiers are also used to request ID types. In that case, only the `system` key of a FHIR Identifier is used:

```json
{
    "system": "pid",
}
```

See the examples below to see this type of FHIR Identifier in action.

### FHIR Patient

Let us check each possible interaction for FHIR Patient.

#### FHIR Patient read

You can read a FHIR Patient using the default id, supplied by the `id` content element of a FHIR Patient. Moreover, each creation and update interaction contains the URL to the Resource. Reading a patient does not need any session or token id and reduces the number of requests needed to read a patient from three to one. [FHIR general parameters](http://hl7.org/fhir/http.html#parameters) may be used to specify and / or modify the response. Let us consider an example, first by supplying the traditional REST API:

```plantuml
@startuml
Client -> Mainzelliste: HTTP Header "mainzellisteApiKey: s3cr3t"\nPOST www.mainzelliste.de/sessions
Mainzelliste -> Client: 201 Created\n{\n    "sessionId": "d81793a2‐bad3‐44d1‐9496‐65ae16814979"\n}
Client -> Mainzelliste: HTTP Header "mainzellisteApiKey: s3cr3t"\nPOST www.mainzelliste.de/sessions/d81793a2‐bad3‐44d1‐9496‐65ae16814979/tokens\n{\n    "type": "readPatients",\n    "data": {\n        "searchIds": [ \n            { "idType": "pid", "idString": "0003Y0WZ" }\n        ],\n        "resultFields": [ "vorname", "nachname", "wohnort" ],\n        "resultIds": [ "pid" ]\n    }\n}
Mainzelliste -> Client: 201 Created\n{\n    "id": "fb1800eb‐9c1b‐46ca‐8e67‐d217ff2888d9",\n    "type": "readPatient"\n}
Client -> Mainzelliste: HTTP Header "mainzellisteApiKey: s3cr3t"\nGET www.mainzelliste.de/patients?tokenId=fb1800eb‐9c1b‐46ca‐8e67‐d217ff2888d9
Mainzelliste -> Client: 200 OK\n[ {\n    "fields": {\n        "vorname": "Marcel",\n        "nachname": "Parciak",\n        "wohnort": "Göttingen"\n    },\n    "ids": [\n        { "idType": "pid", "idString": "0003Y0WZ" }\n    ]\n} ]
@enduml
```

In comparison, this sequence can be performed using this FHIR Request:

```plantuml
@startuml
Client -> Mainzelliste: HTTP Header "mainzellisteApiKey: s3cr3t"\nGET www.mainzelliste.de/fhir/Patient/0003Y0WZ
Mainzelliste -> Client: 200 OK\n{\n    "resourceType": "Patient",\n    "id": "0003Y0WZ",\n    "identifier": [\n        { "system": "pid", "value": "0003Y0WZ" },\n        { "system": "patientennummer", "value": "11111"}\n    ],\n    "name": [\n        { "given": [ "Marcel" ], "family": "Parciak" }\n    ],\n    "address": [\n        { "postalCode": "37099", "city": "Göttingen" }\n    ]\n}
@enduml
```

#### FHIR Patient update

Similar to FHIR read, the FHIR update interaction requires using the default identifier supplied as the [FHIR Logical ID](http://hl7.org/fhir/resource.html#id). This interaction does not require a session or token and may use [FHIR general parameters](http://hl7.org/fhir/http.html#parameters) to modify or format the result. Let us compare the traditional Mainzelliste API call to the FHIR API call:

```plantuml
@startuml
Client -> Mainzelliste: HTTP Header "mainzellisteApiKey: s3cr3t"\nPOST www.mainzelliste.de/sessions
Mainzelliste -> Client: 201 Created\n{\n    "sessionId": "d81793a2‐bad3‐44d1‐9496‐65ae16814979"\n}
Client -> Mainzelliste: HTTP Header "mainzellisteApiKey: s3cr3t"\nPOST www.mainzelliste.de/sessions/d81793a2‐bad3‐44d1‐9496‐65ae16814979/tokens\n{\n    "type": "editPatient",\n    "data": {\n        "patientId": { "idType": "pid", "idString": "0003Y0WZ" },\n        "fields": [ "nachname" ],\n        "ids": [ "patientennummer" ]\n    }\n}
Mainzelliste -> Client: 201 Created\n{\n    "id": "fb1800eb‐9c1b‐46ca‐8e67‐d217ff2888d9",\n    "type": "editPatient"\n}
Client -> Mainzelliste: HTTP Header "mainzellisteApiKey: s3cr3t"\nPUT www.mainzelliste.de/patients/tokenId/fb1800eb‐9c1b‐46ca‐8e67‐d217ff2888d9\nnachname=ParciakNew&patientennummer=123456
Mainzelliste -> Client: 204 No Content
@enduml
```

In contrast, you can use the FHIR API as follows:

```plantuml
@startuml
Client -> Mainzelliste: HTTP Header "mainzellisteApiKey: s3cr3t"\nPUT www.mainzelliste.de/fhir/Patient/0003Y0WZ\n{\n    "resourceType": "Patient",\n    "id": "0003Y0WZ",\n    "identifier": [\n        { "system": "pid", "value": "0003Y0WZ" },\n        { "system": "patientennummer", "value": "12345"}\n    ],\n    "name": [\n        { "given": [ "Marcel" ], "family": "ParciakNew" }\n    ],\n    "address": [\n        { "postalCode": "37099", "city": "Göttingen" }\n    ]\n}
Mainzelliste -> Client: 200 OK\n{\n    "resourceType": "Patient",\n    "id": "0003Y0WZ",\n    "identifier": [\n        { "system": "pid", "value": "0003Y0WZ" },\n        { "system": "patientennummer", "value": "12345"}\n    ],\n    "name": [\n        { "given": [ "Marcel" ], "family": "ParciakNew" }\n    ],\n    "address": [\n        { "postalCode": "37099", "city": "Göttingen" }\n    ]\n}
@enduml
```

#### FHIR Patient create

The FHIR API allows to create patients with FHIR Patient Resources. They have to be sent as a HTTP POST request to the FHIR Patient REST-endpoint. In contrast to FHIR read and FHIR update, no FHIR Logical ID has to be supplied this time (as we are not able to have one so far). Again, let us compare the traditional Mainzelliste API to the FHIR API:

```plantuml
@startuml
Client -> Mainzelliste: HTTP Header "mainzellisteApiKey: s3cr3t"\nPOST www.mainzelliste.de/sessions
Mainzelliste -> Client: 201 Created\n{\n    "sessionId": "d81793a2‐bad3‐44d1‐9496‐65ae16814979"\n}
Client -> Mainzelliste: HTTP Header "mainzellisteApiKey: s3cr3t"\nPOST www.mainzelliste.de/sessions/d81793a2‐bad3‐44d1‐9496‐65ae16814979/tokens\n{\n    "type": "addPatient",\n    "data": {\n        "idtypes": [ "pid", "intid" ],\n    }\n}
Mainzelliste -> Client: 201 Created\n{\n    "id": "fb1800eb‐9c1b‐46ca‐8e67‐d217ff2888d9",\n    "type": "addPatient"\n}
Client -> Mainzelliste: HTTP Header "mainzellisteApiKey: s3cr3t"\nPOST www.mainzelliste.de/patients?tokenId=fb1800eb‐9c1b‐46ca‐8e67‐d217ff2888d9\nvorname=Marcel&nachname=Parciak&wohnort=Göttingen&plz=37099&patientennummer=11111
Mainzelliste -> Client: 201 Created\n[\n    { "idType": "pid", "idString": "0003Y0WZ" },\n    { "idType": "intid", "idString": "1" }\n]
@enduml
```

Using the FHIR API, you can POST a FHIR Patient Resource to archieve the same:

```plantuml
@startuml
Client -> Mainzelliste: HTTP Header "mainzellisteApiKey: s3cr3t"\nPOST www.mainzelliste.de/fhir/Patient\n{\n    "resourceType": "Patient",\n    "identifier": [\n        { "system": "pid" },\n        { "system": "intid" },\n        { "system": "patientennummer", "value": "11111"}\n    ],\n    "name": [\n        { "given": [ "Marcel" ], "family": "Parciak" }\n    ],\n    "address": [\n        { "postalCode": "37099", "city": "Göttingen" }\n    ]\n}
Mainzelliste -> Client: 201 Created\n{\n    "resourceType": "Patient",\n    "id": "0003Y0WZ",\n    "identifier": [\n        { "system": "pid", "value": "0003Y0WZ" },\n        { "system": "intid", "value": "1" },\n        { "system": "patientennummer", "value": "11111"}\n    ],\n    "name": [\n        { "given": [ "Marcel" ], "family": "ParciakNew" }\n    ],\n    "address": [\n        { "postalCode": "37099", "city": "Göttingen" }\n    ]\n}
@enduml
```

Note that by supplying the FHIR Patient Resource without knowing what the default ID will be, you leave out the FHIR Logical ID as well. It will be set and replied by Mainzelliste. Internally, the EPI Linker of Mainzelliste will run and match the new patient to all stored records, just like it would using the traditional Mainzelliste API. Thus, if a possible match is found, the response may look like this:

```plantuml
@startuml
Client -> Mainzelliste: HTTP Header "mainzellisteApiKey: s3cr3t"\nPOST www.mainzelliste.de/fhir/Patient\n{\n    "resourceType": "Patient",\n    "identifier": [\n        { "system": "pid" },\n        { "system": "intid" },\n        { "system": "patientennummer", "value": "11111"}\n    ],\n    "name": [\n        { "given": [ "Marcel" ], "family": "Parciak" }\n    ],\n    "address": [\n        { "postalCode": "37099", "city": "Göttingen" }\n    ]\n}
Mainzelliste -> Client: 201 Created\n{\n    "resourceType": "Patient",\n    "id": "0003Y0WZ",\n    "identifier": [\n        { "use": "temp", "system": "pid", "value": "0003Y0WZ" },\n        { "use": "temp", "system": "intid", "value": "1" },\n        { "system": "patientennummer", "value": "11111"}\n    ],\n    "name": [\n        { "given": [ "Marcel" ], "family": "ParciakNew" }\n    ],\n    "address": [\n        { "postalCode": "37099", "city": "Göttingen" }\n    ]\n}
@enduml
```

You will be still able to use the `temp` FHIR Identifiers, but you should be aware that they may be merged in the future. It is advised to re-check them after some time. You will notice either that a FHIR read will supply the same data without `temp` or your request will automatically redirect you to the new FHIR Patient Resource. 

Keep in mind that you may receive a 409 HTTP status code `Conflict` if there is a mismatch of external identifiers (e.g. two external identifiers given, both are available but assigned to different patients).

#### FHIR Patient search

You may have noticed the FHIR read not being equivalent to the readPatient capabilities of Mainzelliste. The traditional API allows to search for multiple patients using different ID types. In contrast, reading FHIR Patients by their default id is rather restrictive. The FHIR search interaction adds this functionality, allowing to search any FHIR Patient(s) based on FHIR Identifiers. FHIR search always returns a FHIR Bundle; thus, it adds a little more complexity to processing the results. You can add [FHIR general parameters](http://hl7.org/fhir/http.html#parameters) to modify or format the FHIR Bundle response. Let us compare a traditional Mainzelliste API call to its FHIR API variant first:

```plantuml
@startuml
Client -> Mainzelliste: HTTP Header "mainzellisteApiKey: s3cr3t"\nPOST www.mainzelliste.de/sessions
Mainzelliste -> Client: 201 Created\n{\n    "sessionId": "d81793a2‐bad3‐44d1‐9496‐65ae16814979"\n}
Client -> Mainzelliste: HTTP Header "mainzellisteApiKey: s3cr3t"\nPOST www.mainzelliste.de/sessions/d81793a2‐bad3‐44d1‐9496‐65ae16814979/tokens\n{\n    "type": "readPatients",\n    "data": {\n        "searchIds": [\n            { "idType": "patientennummer", "idString": "11111" },\n            { "idType": "intid", "idString": "2" }\n        ],\n        "resultFields": [ "vorname", "nachname", "wohnort" ],\n        "resultIds": [ "pid" ]\n    }\n}
Mainzelliste -> Client: 201 Created\n{\n    "id": "fb1800eb‐9c1b‐46ca‐8e67‐d217ff2888d9",\n    "type": "readPatient"\n}
Client -> Mainzelliste: HTTP Header "mainzellisteApiKey: s3cr3t"\nGET www.mainzelliste.de/patients?tokenId=fb1800eb‐9c1b‐46ca‐8e67‐d217ff2888d9
Mainzelliste -> Client: 200 OK\n[ {\n    "fields": {\n        "vorname": "Marcel",\n        "nachname": "Parciak",\n        "wohnort": "Göttingen"\n    },\n    "ids": [\n        { "idType": "pid", "idString": "0003Y0WZ" }\n    ]\n}, {\n    "fields": {\n        "vorname": "Christian",\n        "nachname": "Schmidt",\n        "wohnort": "Göttingen"\n    },\n    "ids": [\n        { "idType": "pid", "idString": "0007W0W9" }\n    ]\n} ]
@enduml
```

We will add the [FHIR search Elements](http://hl7.org/fhir/search.html#elements) parameters to mimick the behaviour of the `readPatient` Token. The comparable FHIR search request looks like this:

```plantuml
@startuml
Client -> Mainzelliste: HTTP Header "mainzellisteApiKey: s3cr3t"\nGET www.mainzelliste.de/fhir/Patient\n  ?identifier=patientennummer|11111,identifier=intid|2\n  &_elements=name,address
Mainzelliste -> Client: 200 OK\n{\n    "resourceType": "Bundle",\n    "type": "searchset",\n    "entry": [ {\n        "resource": {\n            "resourceType": "Patient",\n            "id": "0003Y0WZ",\n            "identifier": [\n                { "system": "patientennummer", "value": "11111" },\n                { "system": "pid", "value": "0003Y0WZ" },\n                { "system": "intid", "value": "1" }\n            ],\n            "name": [ {\n                "family": "Parciak",\n                "given": [ "Marcel" ]\n            } ],\n            "address": [ {\n                "city": "Göttingen",\n                "postalCode": "37099"\n            } ]\n        },\n        "search": { "mode": "match" }\n    }, {\n        "resource": {\n            "resourceType": "Patient",\n            "id": "0007W0W9",\n            "identifier": [\n                { "system": "patientennummer", "value": "22222" },\n                { "system": "pid", "value": "0007W0W9" },\n                { "system": "intid", "value": "2" }\n            ],\n            "name": [ {\n                "family": "Schmidt",\n                "given": [ "Christian" ]\n            } ],\n            "address": [ {\n                "city": "Göttingen", "postalCode": "37099"\n            } ]\n        },\n        "search": { "mode": "match" }\n    } ]\n}
@enduml
```

The FHIR API adds the possibility to retrieve a [FHIR summary count](http://hl7.org/fhir/search.html#summary). As an example, we can retrieve only the count of the previous request:

```plantuml
@startuml
Client -> Mainzelliste: HTTP Header "mainzellisteApiKey: s3cr3t"\nGET www.mainzelliste.de/fhir/Patient\n  ?identifier=patientennummer|11111\n  &identifier=intid|5\n  &_summary=count
Mainzelliste -> Client: 200 OK\n{\n    "resourceType": "Bundle",\n    "type": "searchset",\n    "total": 2\n}
@enduml
```

The FHIR search may be used to get Identifier to Identifier mappings by limiting retrieved elements:

```plantuml
@startuml
Client -> Mainzelliste: HTTP Header "mainzellisteApiKey: s3cr3t"\nGET www.mainzelliste.de/fhir/Patient\n  ?identifier=patientennummer|11111\n  &_elements=identifier
Mainzelliste -> Client: 200 OK\n{\n    "resourceType": "Bundle",\n    "type": "searchset",\n    "entry": [ {\n        "resource": {\n            "resourceType": "Patient",\n            "identifier": [\n                { "system": "patientennummer", "value": "11111" },\n                { "system": "pid", "value": "0003Y0WZ" },\n                { "system": "intid", "value": "1" }\n            ]\n        },\n        "search": { "mode": "match" }\n    } ]\n}
@enduml
```

### FHIR Encounter

[FHIR Encounter](http://hl7.org/fhir/encounter.html) represents any Identifier with a `Patient 1:n Identifier` relation, like encounters. As there are no other Resources implemented in Mainzelliste, it is advised to use Encounters to even if the identifiers themselves represent other entities, like laboratory identifiers or observation identifiers. Note that this behaviour may change in the future if we see this feature is needed. In other words: let us know if you need it!

We added a constraint to any [FHIR Encounter](http://hl7.org/fhir/encounter.html) supplied to Mainzelliste: [FHIR Encounter.subject](http://hl7.org/fhir/encounter-definitions.html#Encounter.subject) must contain a reference to a FHIR Patient contained in the same Mainztelliste instance. It is not possible to supply an Encounter that is not related to any FHIR Patient. [FHIR Encounter.status](http://hl7.org/fhir/encounter-definitions.html#Encounter.status), as it is mandatory, will always be set to `unknown`, regardless of any value supplied to Mainzelliste. [FHIR Encounter.class](http://hl7.org/fhir/encounter-definitions.html#Encounter.class), as it is mandatory, will always be set to `IMP` (for in-patient encounter), regardless of any value supplied to Mainzelliste.

#### FHIR Encounter read

`TODO`

#### FHIR Encounter update

`TODO`

#### FHIR Encounter create

`TODO`

#### FHIR Encounter search

`TODO`

### FHIR Bundle

`TODO`
